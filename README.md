# PSEUDOCODE

## An introduction to computers and programming
## Pseudocode 

**The pseudocode lessons are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode)**

**The slides for pseudocode are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/slides/#/)**

**The gitlab exercises should be downloaded from [here](https://gitlab.com/39iosdev/ccd-iqt/idf/pseudocode/-/blob/master/mdbook/src/Labs/labs.md)**  

The graded labs are found in mdbook/src/Labs/YourLastName_FirstInitial_PseudocodeLabs.md 


This unit introduces students to the foundational ideas of computer science.
In this unit, students will review the fundamental technologies and concepts underlying computer science.

The primary focus is familiarizing students with the logic and structures that computer science is used to solve - without relying on a particular syntax.


