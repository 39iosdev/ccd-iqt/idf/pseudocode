## Performing Arithmetic Operations

Standard arithmetic operators:
+ (plus sign)—addition
− (minus sign)—subtraction
* (asterisk)—multiplication
/ (slash)—division
% (percent sign)-remainder

**Rules of precedence**
* Expressed as Order of operations
* States the order in which operations are carried out
* Expressions within parentheses are evaluated first
* All the arithmetic operators have left-to-right associativity
* Multiplication and division are evaluated next
  * From left to right
* Addition and subtraction are evaluated next
  * From left to right

### Precedence & Associativity of Five Common Operators
![](../assets/precedence3.jpg)

Programming Logic & Design (2017), 9th edition
