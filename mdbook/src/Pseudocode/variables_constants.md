## Declaring and Using, Variables and Constants

**Understanding Data Types**
* Data type describes:
  * What values can be held by the item
  * How the item is stored in memory
  * What operations can be performed on the item

* All programming languages support these data types:
  * Numeric consists of numbers that can be used in math
  * String is anything not used in math

---

## Understanding Unnamed, Literal Constants

**There are two types of constants**
* Numeric constant (or literal numeric constant)
* Contains numbers only
* Number does not change

**String constant (or literal string constant)**
* Also known as Alphanumeric values
* Can contain both alphabetic characters and numbers
* Strings are enclosed in quotation marks

## Working with Variables

**Variable are named memory locations** 

**Contents can vary or differ over time**

**Declaration is a statement that provides a variable's:**
* Data type 
* Identifier (variable’s name)
* Optionally, an initial value

### Flowchart and pseudocode of number-doubling program with variable declarations

![](../assets/flowchart1.jpg)

Programming Logic & Design (2017), 9th edition

---

**Casing conventions**

* Variable names that start with a lowercase character.  If a second part is added to the name it should start with an Uppercase character.  Example: **h**ourly**W**age. Commonly referred to as (Camel case)
* Variable names where the first letter in each word in uppercase such as **H**ourly**W**age (Pascal casing)
* A form of camel casing in which the data type is part of the name such as **num**HourlyWage (Hungarian casing)

**Other examples of casing types**
* Parts of variable names are separated by underscores such as hourly **_** wage (snake casing)
* Similar to snake casing, but new words start with a uppercase letter such as **H**ourly_**W**age (mixed case)
* Parts of variable names are separated by dashes such as hourly **-** wage (Kebob case)

## Assigning Values to Variables

**Assignment statement**
* set myAnswer = myNumber * 2

**Assignment operator**
* Equal sign 
* A binary operator, meaning it requires two operands—one on each side     
* Always operates from right to left, which means that it has right-associativity or right-to-left associativity
* The result to the left of an assignment operator is called an lvalue  **Example: operand2 (lvalue) = operand1 * X **

## Initializing a Variable

**Initializing the variable - declare a starting value**

```
num yourSalary = 22.57 
string yourName = “Larry”
```

**Garbage – a variable’s unknown value**

**Variables must be declared before they are used in the program**

## Declaring Named Constants

**Named constant** 
* Similar to a variable
* Can be assigned a value only once
* Assign a useful name to a value that will never be changed during a program’s execution

**Magic number** 
Unnamed constant
* Use
```
taxAmount = price * SALES_TAX_AMOUNT 
```

**instead of:**
```
taxAmount = price * .06
```
