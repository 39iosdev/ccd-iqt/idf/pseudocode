## Pseudocode

### **Introduction:**
This lesson will walk you through how to use pseudo code to properly create flow charts, which is. Utilizing Pseudocode flow charts helps you organize and create efficient algorithms before you ever use a coding/scripting language. 

### **Topics Covered:**

* [**Declaring and Using Variables and Constants**](variables_constants.md#variables-constants)
* [**Arithmetic Operations**](arithmetic_operations.md#arithmetic-operations)
* [**Hierarchy Charts**](hierarchy_charts.md#hierarchy-charts)
* [**Program Design**](program_design.md#program-design)
* [**Modularizing a program**](modularizing.md#modularizing)
* [**Flowchart**](flowchart.md#flowchart)


#### To access the Pseudocode slides please click [here](slides)