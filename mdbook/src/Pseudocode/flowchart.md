### Statements and Flowchart Symbols

**Pseudocode**
* English-like representation of the logical steps it takes to solve a problem

**Flowchart**
* Pictorial representation of the logical steps it takes to solve a problem

## Writing Pseudocode

**Pseudocode representation of a number-doubling problem**

```
start
   input myNumber
   set myAnswer = myNumber * 2
   output myAnswer
stop

```

## Pseudocode Standards

* Programs begin with the word start and end with the word stop; these two words are always aligned
* Whenever a module name is used, it is followed by a set of parentheses
* Modules begin with the module name and end with return.  The module name and return are always aligned
* Each program statement performs one action

**Example** input, processing, or output
* Program statements are indented a few spaces more than the word start or the module name
* Each program statement appears on a single line if possible. When this is not possible, continuation lines are indented
* Program statements begin with lowercase letters
* No punctuation is used to end statements
---

## Drawing Flowcharts

**Create a Flowchart**
* Draw geometric shapes that contain the individual statements
* Connect shapes with arrows

**Input Symbol**
* Indicates input operation
* Parallelogram

![](../assets/input1.jpg)

**Processing symbol**
* Contains processing statements such as arithmetic
* Rectangle

![](../assets/mynumb2.jpg)

**Output symbol**
* Represents output statements
* Parallogram

![](../assets/MyAnsw3.jpg)

**Flowlines**
* Arrows that connect steps

**Terminal symbols**
* Start/stop symbols
* Shaped like a racetrack
* Called lozenges

![](../assets/StartStop4a.png)

![](../assets/flowChartDoub.png)

Programming Logic & Design (2017), 9th edition

---

## End a Program

**Making a Decision**
* Testing a value
* Decision symbol
  * Diamond shape
  
**Dummy value**
* Data-entry value that the user will never need
* Sentinel value

**End of File (eof)**
* Marker at the end of a file that automatically acts as a sentinel

![](../assets/flowChartSent.png)

Programming Logic & Design (2017), 9th edition

---
