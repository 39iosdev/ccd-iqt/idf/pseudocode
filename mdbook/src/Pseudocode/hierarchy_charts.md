## Creating Hierarchy Charts

**Hierarchy chart**
* Shows the overall picture of how modules are related to one another
* Defines which modules exist within a program and which modules call others
* A Specific module may be called from locations within a program

**Planning tool** 
* Develop the overall relationship of program modules before you write them 

**Documentation tool**

### Hierachy chart of payroll report program
![](../assets/higharchy4.jpg)

Programming Logic & Design (2017), 9th edition

---
### Billing program hiearchy chart
![](../assets/higharchy5.jpg)

Programming Logic & Design (2017), 9th edition
