## Features of Good Program Design

* **Use program comments where appropriate**
* **Identifiers should be chosen carefully**
* **Strive to design clear statements within your programs and modules**
* **Write clear prompts and echo input**
* **Continue to maintain good programming habits as you develop your programming skills**

## Maintaining Good Programming Habits

**Every program you write will be better if you:** 
* Plan prior to coding
* Continue to draw flowcharts or write pseudocode
* Draft your program logic on paper
* Think carefully about the variable and module names you use
* Design your program statements for ease of reading and use

---

### Understanding the Program Development Cycle

**Program development cycle**
* Understand the problem
* Plan the logic
* Code the program
* Use software (a compiler or interpreter) to translate the program into machine language
* Test the program
* Put the program into production
* Maintain the program

---

   ## Program Development Cycle

![](../assets/Dev_Cycle.jpg)

Programming Logic & Design (2017), 9th edition

---
## Using Software to Translate the Prgram into Machine Language

![](../assets/machLang.png)

Programming Logic & Design (2017), 9th edition

---
