## Intro to Programming

### **Introduction:**
This lesson is a brief introduction to programming and computer systems. We will briefly go over and discuss a variety of computer components, how to use and perform operations on binary, decimal, and hexadecimal numbers.

### **Topics Covered:**

* [**Understanding Computer Systems**](overview.md#overview)
* [**Binary, Decimal, Hex**](binary_decimal_hex.md#binary-decimal-hex)

#### To access the Intro to Programming slides please click [here](slides)
