## Binary

**The representing the digital values of "1's" (one's) and "0's" (zero's) are commonly called: *BI*nary digi*TS*, and in digital and computational circuits and applications they are normally referred to as binary _BITS_.**

* The binary number system is a Base-2 numbering system which follows the same set of rules in mathematics as the commonly used decimal or base-10 number system. So instead of powers of ten, (10n) for example: 1, 10, 100, 1000 etc, binary numbers use powers of two, (2n) effectively doubling the value of each successive bit as it goes, for example: 1, 2, 4, 8, 16, 32 etc.
* As the decimal number is a weighted number, converting from decimal to binary (base 10 to base 2) will also produce a weighted binary number with the right-hand most bit being the Least Significant Bit or LSB, and the left-hand most bit being the Most Significant Bit or MSB, and we can represent this as:

| Number of Binary Digits (bits) | Common Name |
|:------------------------------:|:-----------:|
|                1               |     Bit     |
|                4               |    Nibble   |
|                8               |     Byte    |
|               16               |     Word    |
|               32               | Double Word |
|               64               |  Quad Word  |

**Representing a Binary number**

| MSB |               |       |   |       |       |       |       | LSB  |
|:---:|:------------: |:----: |:----: |:----: |:----: |:----: |:----: |:---: |
|  2<sub>8</sub>| 2<sub>7</sub>| 2<sub>6</sub>| 2<sub>5</sub>| 2<sub>4</sub>| 2<sub>3</sub>| 2<sub>2</sub>| 2<sub>1</sub>|  2<sub>0</sub>|
| 256  |      128      | 64  | 32  | 16  |  8  |  4  |  2  |  1   |

* Then the first digit has a weight of 1, the second digit has a weight of 2, the third a weight of 4, the fourth a weight of 8 and so on.

**Conveting Binary to Decimal**

| Decimal Digit Value  | 256  | 128  | 64  | 32  | 16  | 8  | 4  | 2  | 1  |
|:-------------------: |:---: |:---: |:--: |:--: |:--: |:-: |:-: |:-: |:-: |
|  Binary Digit Value  |  1   |  1   |  1  |  1  |  1  | 1  | 1  | 1  | 1  |

* By adding together ALL the decimal number values from right to left at the positions that are represented by a "1" gives us our base 10 representation of our binary number:
* 256 + 128 + 64 + 32 + 16 + 8 + 4 + 2 + 1 = 511<sub>10</sub>

**Converting Decimal to Binary**

* An easy method of converting decimal to binary number equivalents is to write down the decimal number and to continually divide-by-2 (two) to give a result and a remainder of either a "1" or a "0" until the final result equals zero.
* Starting with 357<sub>10</sub>

|357<sub>10</sub>|       |              |        |
|------------: |:-: |:---------: |:-------: |
| divide by 2  |    |            |          |
|      result  | 178  | remainder  | 1  (LSB)  |
| divide by 2  |    |            |          |
|      result  | 89  | remainder  | 0       |
| divide by 2  |    |            |          |
|      result  | 44  | remainder  | 1       |
| divide by 2  |    |            |          |
|      result  | 22  | remainder  | 0       |
| divide by 2  |    |            |          |
|      result  | 11  | remainder  | 0      |
| divide by 2  |    |            |          |
|      result  | 5  | remainder  | 1       |
| divide by 2  |    |            |          |
|      result  | 2  | remainder  | 1      |
| divide by 2  |    |            |          |
|      result  | 1  | remainder  | 0       |
| divide by 2  |    |            |          |
|      result  | 0  | remainder  | 1  (MSB)  |

* Resulting in 101100101<sub>2</sub>

---

## Hexadecimal (Hex)

**The Hexadecimal or simply Hex numbering system uses the Base of 16**

* Being a Base-16 system, the hexadecimal numbering system therefore uses 16 (sixteen) different digits with a combination of numbers from 0 through to 15. In other words, there are 16 possible digit symbols.
* The hexadecimal numbers that identify the values of ten - fifteen are replaced with capital letters of A, B, C, D, E and F respectively.

|  Decimal  |    Binary    |     Hex     |
|:--: |:---------: |:--------: |
|  1  |    0001    |     1     |
|  2  |    0010    |     2     |
|  3  |    0011    |     3     |
|  4  |    0100    |     4     |
|  5  |    0101    |     5     |
|  6  |    0110    |     6     |
|  7  |    0111    |     7     |
|  8  |    1000    |     8     |
|  9  |    1001    |     9     |
| 10  |    1010    |     A     |
| 11  |    1011    |     B     |
| 12  |    1100    |     C     |
| 13  |    1101    |     D     |
| 14  |    1110    |     E     |
| 15  |    1111    |     F     |
| 16  | 0001 0000  | 10 (1+0)  |
| 20  | 0001 0100  | 14 (1+4)  |
| 26  | 0001 1010  | 1A (1+10) |

**Why use HEX?**

* Easier and more compact representation of larger binary digits
* ie. 1000 1111 1101 1001<sub>2</sub> = #8FD9<sub>16</sub>
