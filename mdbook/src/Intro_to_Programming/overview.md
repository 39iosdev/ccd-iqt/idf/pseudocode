
### Understanding Computer Systems

**Computer system**

* Combination of hardware and software components, that communicate with each other and externally, required to process and store data using a computer

**Hardware**

* Physical, tangible parts or components (equipment) used to with a computer
  
**Software**

* A set of data or computer instructions that tells the computer how to work
* Programs are a collection of instructions written by programmers that performs a specific task when executed by a computer
* Programming is the process of taking an algorithm or a programming languages code and using it to write software instructions
* Application software such as word processing, spreadsheets, payroll and inventory, even games
* System software such as operating systems like Windows, Linux, or UNIX, Google Android and Apple IOS

---
**Computer hardware and software accomplish three major operations**

* Input is typically data items such as text, numbers, images, and sound
* Processing, where calculations and comparisons are performed by the central processing unit (CPU)
* Output where the resulting information is sent to a printer, a monitor, or storage devices after processing

**Programming language**

* Used to write computer instructions called program code
* Writing instructions is called coding the program
* Examples: C, C++, Python, Java
  
**Syntax**

* A set of rules specific to a language, governing programming language usage, arithmatic, and punctuation
* Mistakes in a language’s usage are syntax errors

**Computer memory**

* Computer’s temporary, internal storage – random access memory (RAM)
* Volatile memory – lost when the power is off

---

**Permanent storage devices**

* Nonvolatile memory
* Hard Disk Drive (HDD) or Solid State Drive (SSD)

**Compiler or interpreter**

* Translates source code into machine language (binary language) statements called object code
* Checks for syntax errors
* Programs with syntax cannot execute

**Program executes or runs**

* Input will be accepted, some processing will occur, and results will be output

---

**Logical errors**

* Errors in program logic produce incorrect output

**Logic of the computer program**

* Sequence of specific instructions in specific order

**Variable** 

* Named memory location whose value can vary
