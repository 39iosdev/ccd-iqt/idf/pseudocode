## Logical Structures

### **Introduction:**
In this lesson, we will go through an introduction to logical structures that will be used throughout not only this course but also your journey as a developer. At the basis of every piece of software is a skeleton of logic that is dressed up with syntax, and here we will begin to learn how to use logic for making decisions and going through iterations.

### **Topics Covered:**

* [**Structures**](structures.md#structures)
* [**If...THEN...Else**](if_then_else.md#if-then-else)
* [**Loops**](loops.md#loops)
* [**Arrays**](arrays.md#arrays)


#### To access the Logic slides please click [here](slides)
