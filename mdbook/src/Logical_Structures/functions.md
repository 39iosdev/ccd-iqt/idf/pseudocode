## Functions

### **Introduction:**
In this lesson, we will go through how to break your code apart by the various functionalities you are tryin to accomplish with your piece of code. It is imporatant to break apart your code by functions to avoid creating a mess that not only cant be interpreted/compiled but also isn't easy to be read and debugged.

### **Topics Covered:**

* [**Pass By Reference**](pass_by_ref.md#structures)
* [**Pass By Value**](pass_by_value.md#if-then-else)
* [**Input Validation**](input_validation.md#loops)
