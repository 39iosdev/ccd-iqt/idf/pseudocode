## **Lesson Objectives:**
* **LO 1** Demonstrate the use of logic in conditional statements (Proficiency Level: C)
    * **MSB 1.1** Explain the use of logical operators (Proficiency Level: C)
    * **MSB 1.2** Explain the use of relational operators (Proficiency Level: C)
  
* **LO 2** Demonstrate how to construct conditional statement blocks in both pseudocode and flowchart form (Proficiency Level: B)

* **LO 3** Demonstrate the use of loops in a pseudocode program (Proficiency Level: B)

* **LO 4** Distinguish between the three basic programming structures (Proficiency Level: B)
    * **MSB 4.1** Utilize a sequential structure in a pseudocode program (Proficiency Level: B)
    * **MSB 4.2** Utilize a Looping Structure in a pseudocode program (Proficiency Level: B)
    * **MSB 4.3** Utilize a Conditional Structure in a pseudocode program (Proficiency Level: B)
* **LO 5** Select the appropriate control structure for a pseudocode problem (Proficiency Level: B)

* **LO 6** Explain the utility of an algorithm written in pseudocode (Proficiency Level: B)

## **Performance Objectives (Proficiency Level: 3c)**
* **Conditions:** Given access to (references, tools, etc.):
    * Access to specified remote virtual environment
    * Student Guide and Lab Guide
    * Student Notes


* **Performance/Behavior Tasks:**
    * Utilize conditional statements to solve a problem in pseudocode
    * Use logical operators to build a conditional statement
    * Utilize loops to solve a problem in pseudocode and flowchart format
    * Develop a solution to solve a problem using conditional structures.
    * Develop a solution to solve a problem conditional looping structures.
    * Develop a solution to solve a problem using sequential structures

* **Standard(s)**
    * **Criteria:** Demonstration: Correctable to 100% in class
    * **Evaluation:** Students will have 4 hours to complete the timed evaluation consisting of both cognitive and performance components.
    * Minimum passing score is 80%